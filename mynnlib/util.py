import math


# a: neuron's pre-activation output
# o: neuron's post-activation output
# activation function collection, o = Activation(a)
class Activation:
    @staticmethod
    def get_func(func_name):
        return eval("Activation." + func_name)

    @staticmethod
    def sigmoid(a):
        return 1 / (1 + math.exp(-a))


# derivative of activation functions, do/da = DActivation(o)
class DActivation:
    @staticmethod
    def get_func(func_name):
        return eval("DActivation." + func_name)

    @staticmethod
    def sigmoid(o):
        return o * (1 - o)


# y = [y1, ...]: label vector
# o = [o1, ...]: post-activation output of Model's output neurons
# ls: loss value
# loss function collection, ls = Loss(y, o1, o2, ...)
class Loss:
    @staticmethod
    def get_func(func_name):
        return eval("Loss." + func_name)

    @staticmethod
    def squared(y, o):
        if len(y) != len(o):
            raise Exception("label dimension not equal to Model's output dimension!")
        ls = 0
        for i in range(len(y)):
            ls += ((y[i] - o[i]) ** 2)
        return ls


# o: post-activation output of Model's output neuron
# y: the corresponding label element
# derivative of loss functions, dls/do = DLoss(o)
# called on output neurons 1 by 1
class DLoss:
    @staticmethod
    def get_func(func_name):
        return eval("DLoss." + func_name)

    @staticmethod
    def squared(y, o):
        return 2 * (y - o)



