import random
import csv

n_data = 500
n_input = 3
n_output = 4

if __name__ == '__main__':
    random.seed()

    datas = []
    for i in range(n_data):
        d = []
        for j in range(n_input+n_output):
            d.append(random.random())
        datas.append(d)

    with open('sample_data.csv', 'w') as f:
        fwriter = csv.writer(f)
        fwriter.writerows(datas)
