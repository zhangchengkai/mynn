import random
from mynnlib.util import *


class Synapse:
    def __init__(self, p, n):
        self.w = random.random()  # weight
        self.p = p  # prev neuron
        self.n = n  # next neuron


class Neuron:
    def __init__(self):
        self.p = []  # prev synapses
        self.n = []  # next synapses
        self.o = 0  # neuron post-activation output / For input neuron, it's simply the input value or 1 (for bias).
        self.d = 0  # delta
        self.actv = None  # activation func
        self.dact = None  # derivative of activation func

    def set_actv_name(self, actv_func):
        self.actv = Activation.get_func(actv_func)
        self.dact = DActivation.get_func(actv_func)

    # Forward Propagation.
    # Use recurse, so it's only explicitly called on output neurons.
    def update_o(self):
        if not self.p:  # no prev, input neuron, needn't update
            return
        self.o = 0
        for synp in self.p:
            prev_nrn = synp.p
            prev_nrn.update_o()
            self.o += prev_nrn.o * synp.w
        self.o = self.actv(self.o)

    # Backward Propagation.
    # @param dloss: only needed when this func is called on output neurons. dloss(o) = dL/do
    def update_d(self, dloss=None):
        if not self.n:  # no next, output neuron, delta = dL/do * do/da (L:loss, o: neuron post-activation output, a:neuron pre-activation output)
            self.d = dloss(self.o) * self.dact(self.o)
            return
        raise NotImplementedError()


class Model:
    def __init__(self):
        self.i = []  # input neurons
        self.o = []  # output neurons
        self.ls = None  # loss func
        self.dl = None  # derivative of loss func

    def create_from_file(self, f):
        raise NotImplementedError()

    def save_to_file(self, f):
        raise NotImplementedError()

    def load_from_file(self, f):
        raise NotImplementedError()

    def train(self, lr):  # lr: learning rate
        raise NotImplementedError()


if __name__ == '__main__':
    random.seed()


